<?php

require_once 'vendor/autoload.php';

use controller\Temperature\Temperature;
use controller\Template\Template; 

$temperature = new Temperature();
$view = new Template();

$dataList = $temperature->getTemperatureFilter('Moscow', $_GET);

$view->setVariable('dataList', $dataList);


$view->setExternalJS(['https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js']);
$view->setJS(['js/findMaxMin'],false);
$view->loadPage('index');

