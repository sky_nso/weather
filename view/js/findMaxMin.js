
function findMaxMin(){
    var tempList = [];

    $(".temperature").each(function(key, value){
        tempList[key] = $(value).text();
    });

    var max = getMaxOfArray(tempList);
    var min = getMinOfArray(tempList);

    $(".temperature").each(function(key, value){
        if($(value).text() == max && max != false) {
            $(".temperature").eq(key).css('color', 'red');
            max = false;
        }

        if($(value).text() == min && min != false) {
            $(".temperature").eq(key).css('color', 'blue');
            min = false;
        }
    });

}

function getMaxOfArray(numArray) {
    return Math.max.apply(null, numArray);
}

function getMinOfArray(numArray) {
    return Math.min.apply(null, numArray);
}





