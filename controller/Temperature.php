<?php

/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 06.12.2016
 * Time: 23:46
 */

namespace controller\Temperature;

use controller\City\City;
use model\DataBase\DataBase;
use model\DataFormater\DataFormater;

class Temperature
{

    public $error;

    function __construct()
    {
        $this->error = '';
    }

    public function addTemperatureInfo($value, $city, $units)
    {
        $db = new DataBase();

        $cityModel = new City();

        if (!$cityId = $cityModel->getID($city)) {
            $this->error = 'Error get city';
            return false;
        }

        $units = $this->getSystemUnits($units);

        if (!$this->unitsValidate($units)) {
            $this->error = 'Error units';
            return false;
        }

        $db->query("INSERT INTO " . $this->tableName() . " (`$units`, `city_id`, `date`) VALUES($value, $cityId, " . time() . ");");

        if ($sqlError = $db->getError() != '') {
            $this->error = "Error SQL: " . $sqlError;
            return false;
        }

        return true;
    }

    public function getSystemUnits($unit)
    {
        $data = [
            'imperial' => 'fahrenheit',
            'metric' => 'celsius',
        ];

        if (isset($data[$unit])) {
            return $data[$unit];
        }

        return false;
    }

    public function unitsValidate($units = '')
    {
        $data = [
            'celsius',
            'fahrenheit'
        ];

        return in_array($units, $data);
    }

    public function tableName()
    {
        return 'temperatureHistory';
    }

    public function getLastValue($city, $units)
    {

        $db = new DataBase();

        if (!$cityId = City::getID($city)) {
            $this->error = 'Error get city';
            return false;
        }

        if (!$colum = $this->getSystemUnits($units)) {
            $this->error = 'Error: unknown unit ';
            return false;
        }

        $result = $db->one("SELECT $colum FROM " . $this->tableName() . " WHERE `city_id` = $cityId  ORDER BY `id` DESC;");

        if ($result != '') {
            return $result;
        }

        return false;
    }

    public function getTemperatureByPeriod($city, $from, $to, $units = 'metric')
    {
        if (!$cityId = City::getID($city)) {
            $this->error = 'Error get city';
            return false;
        }

        $dateFrom = DataFormater::getTimestamp($from);
        $dateTo = DataFormater::getTimestamp($to);

        if (!$colum = $this->getSystemUnits($units)) {
            $this->error = 'Error: unknown unit ';
            return false;
        }

        $db = new DataBase();

        $result = $db->all('SELECT ' . $colum . ', date FROM ' . $this->tableName() . ' WHERE `date` BETWEEN ' . $dateFrom . ' AND ' . $dateTo . ' AND `city_id` = ' . $cityId);

        if (empty($result)) {
            $this->error = 'Error: empty result';
            return false;
        }

        foreach ($result as $key => $value) {
            $result[$key]['date'] = DataFormater::getFormatDate($value['date']);
        }

        return $result;

    }

    public function getTemperatureFilter($city, $filter = [], $units = 'metric')
    {
        if (!$cityId = City::getID($city)) {
            $this->error = 'Error get city';
            return false;
        }

        if (!$colum = $this->getSystemUnits($units)) {
            $this->error = 'Error: unknown unit ';
            return false;
        }

        $query = '';
        $orderBy = '';

        if (!empty($filter)) {

            foreach ($filter as $key => $value) {

                if ($key == 'dateFrom' && !empty($value)) {
                    $query .= ' AND `date` >= ' . DataFormater::getTimestamp($value);
                }

                if ($key == 'dateTo' && !empty($value)) {
                    $query .= ' AND `date` <= ' . DataFormater::getTimestamp($value);
                }

                if ($key == 'orderBy' && is_array($value) && !empty($value[0]) && !empty($value[1])) {

                    $orderBy = ' ORDER BY ' . $value[0] . ' ' . $value[1];
                }

            }

        }

        $db = new DataBase();

        $result = $db->all('SELECT ' . $colum . ', date FROM ' . $this->tableName() . ' WHERE `city_id` = ' . $cityId . $query . $orderBy);

        if (empty($result)) {
            $this->error = 'Error: empty result';
            return false;
        }

        foreach ($result as $key => $value) {
            $result[$key]['date'] = DataFormater::getFormatDate($value['date']);
        }

        return $result;


    }

}

