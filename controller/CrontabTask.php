<?php

/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 06.12.2016
 * Time: 23:37
 */

namespace controller\CrontabTask;

use model\Weather\Weather;
use controller\Temperature\Temperature;

class CrontabTask
{
    public $error;

    public function getTemperatureInfo($city, $units)
    {
        $weather = new Weather();

        $weather->setApiKey();

        $weather->getWeather($city, $units);

        $temperature = new Temperature();

        if ($temperature->addTemperatureInfo($weather->getTemperatureValue(), $city, $units)) {
            return true;
        }

        if($temperature->error != ''){
            $this->error = $temperature->error;
        }

        return false;
    }
}