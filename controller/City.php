<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 07.12.2016
 * Time: 1:37
 */

namespace controller\City;

use model\DataBase\DataBase;

class City
{
    public function getID($name = '') {
        $db = new DataBase();

        return $db->one("SELECT `id` FROM ".self::tableName()." WHERE `name` = '".$name."'");
    }

    static public function tableName()
    {
        return 'city';
    }
}