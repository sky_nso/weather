<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 07.12.2016
 * Time: 3:30
 */

namespace controller\Template;

use model\View\View;

class Template
{
    const PATH = 'view/';
    const PATH_MAIN_CSS = 'view/css/';
    const PATH_MAIN_JS = 'view/js/';
    public $template;
    public $variable = [];
    private $css = [];
    private $js = [];

    public function loadPage($template)
    {
        $this->getHeader();

        $this->setTemplate($template);

        $this->render();

        $this->getBottom();
    }

    private function getHeader()
    {
        $this->setTemplate('main/header');

        $this->setCSS();
        $this->setJS();


        $this->setVariable('_headerCSS', $this->css);
        $this->setVariable('_headerJS', $this->js);

        $this->render();
    }

    public function setCSS($path = [], $withMain = true)
    {
        if ($withMain) {
            $this->css[] = $this->generateTagCSS($this->getPath('css') . 'main.css');
        }

        if (!empty($path)) {
            foreach ($path as $value) {
                $this->css[] = $this->generateTagCSS($this->getPath() . $value . '.css');
            }
        }
    }

    private function generateTagCSS($path, $attributes = [])
    {
        $attr = '';

        foreach ($attributes as $key => $value) {
            $attr .= $key . '="' . $value . '" "';
        }


        return '<link rel="stylesheet" type="text/css" href="' . $path . '" ' . $attr . '>';
    }

    public function getPath($type = '')
    {
        if ($type == 'js') {
            return '/' . self::PATH_MAIN_JS;
        } else if ($type == 'css') {
            return '/' . self::PATH_MAIN_CSS;
        } else {
            return '/' ;
        }

    }

    public function setJS($path = [], $withMain = true)
    {
        if ($withMain) {
            $this->js[] = $this->generateTagJS($this->getPath('js') . 'main.js');
        }

        if (!empty($path)) {
            foreach ($path as $value) {
                $this->js[] = $this->generateTagJS($this->getPath() . self::PATH . $value . '.js');
            }
        }
    }

    private function generateTagJS($path, $attributes = [])
    {
        $attr = '';

        foreach ($attributes as $key => $value) {
            $attr .= $key . '="' . $value . '" "';
        }

        return '<script  type="text/javascript" src="' . $path . '" ' . $attr . '></script>';
    }

    public function setVariable($variable, $value)
    {
        $this->variable[$variable] = $value;
    }

    public function render()
    {
        $view = new View();

        foreach ($this->variable as $key => $value) {
            $view->assign($key, $value);
        }

        $view->loadTemplate($this->getTemplate());

    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function setTemplate($template = '')
    {
        $this->template = $template;
    }

    private function getBottom()
    {
        $this->setTemplate('main/bottom');

        $this->render();
    }

    public function setExternalJS($path = [])
    {
        if (!empty($path)) {
            foreach ($path as $value) {
                $this->js[] = $this->generateTagJS($value);
            }
        }
    }

    /**
     * @return array
     */
    public function checkTemplateExists($template)
    {
        $view = new View();

        return $view->checkTemplateExists($template);
    }

    private function getUrl()
    {
        $url = @($_SERVER["HTTPS"] != 'on') ? 'http://' . $_SERVER["SERVER_NAME"] : 'https://' . $_SERVER["SERVER_NAME"];
        $url .= ($_SERVER["SERVER_PORT"] != 80) ? ":" . $_SERVER["SERVER_PORT"] : "";
        $url .= $_SERVER['SERVER_NAME'];
        return $url;
    }

}