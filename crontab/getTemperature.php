<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . "autoload.php";

use model\Logger\Logger;
use controller\CrontabTask\CrontabTask;

try {

    $Crontab = new CrontabTask();
    if ($Crontab->getTemperatureInfo('Moscow', 'metric')) {
        Logger::saveLog('OK');
    } else {
        if ($Crontab->error != '') {
            $error = $Crontab->error;
        } else {
            $error = 'Unknow Error';
        }

        Logger::saveLog($error, true);
    }

} catch (Exception $e) {
    Logger::saveLog($e->getMessage(), true);
}

