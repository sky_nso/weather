<?php

/**
 * Created by PhpStorm.
 * User: paraf_000
 * Date: 06.12.2016
 * Time: 21:35
 */

namespace model\Weather;

use model\Config\Config;

class Weather
{
    private $url = 'http://api.openweathermap.org/data/2.5/weather?';
    private $apiKey = '';
    private $mode = 'json';

    public $dataResponse;

    private function buildUrl($query, $units, $lang, $url)
    {
        $queryUrl = $this->buildQueryUrl($query);

        $url = $url . $queryUrl . "&units=$units&lang=$lang&mode=$this->mode&APPID=" . $this->apiKey;

        return $url;
    }

    private function buildQueryUrl($query)
    {
        return 'q=' . urlencode($query);
    }

    public function __construct($apiKey = '')
    {
        if (is_string($apiKey) || !empty($apiKey)) {
            $this->apiKey = $apiKey;
        }
    }

    public function setApiKey($apiKey = '')
    {
        if($apiKey != '') {
            $this->apiKey = $apiKey;
        } else {
            $config = Config::getConfigAPI();
            $this->apiKey = $config['key'];
        }
    }

    public function getWeather($query, $units = 'metric', $lang = 'ru', $appid = '')
    {
        $answer = $this->getDataWeather($query, $units, $lang, $appid, $this->mode);
        $this->dataResponse = json_decode($answer);

        return $this->parsJSON($answer);
    }

    public function getDataWeather($query, $units = 'metric', $lang = 'ru')
    {
        $url = $this->buildUrl($query, $units, $lang, $this->url);

        return $this->getData($url);
    }

    public function getData($url)
    {
        return file_get_contents($url);
    }

    public function getTemperatureInfo()
    {
        if (empty($this->dataResponse))
            return false;

        return $this->dataResponse->main;
    }

    public function getTemperatureValue()
    {
        $temperature = $this->getTemperatureInfo();

        if (!empty($temperature)) {
            return $temperature->temp;
        }

        return false;
    }

    public function parsJSON($answer){
        return json_decode($answer, true);
    }

}