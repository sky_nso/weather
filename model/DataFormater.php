<?php
/**
 * Created by PhpStorm.
 * User: paraf_000
 * Date: 07.12.2016
 * Time: 16:08
 */

namespace model\DataFormater;


class DataFormater
{
    public static function getTimestamp($date)
    {
        return strtotime($date);
    }

    public static function getFormatDate($timestamp)
    {
        return date('Y-m-d', $timestamp);
    }

    public static function getTime($timestamp)
    {
        return date('H:i:s', $timestamp);
    }

    public static function getYear($timestamp)
    {
        return date('Y', $timestamp);
    }

    public static function getMonth($timestamp)
    {
        return date('m', $timestamp);
    }

    public static function getDate($timestamp)
    {
        return date('d', $timestamp);
    }

    public static function getHour($timestamp)
    {
        return date('H', $timestamp);
    }

    public static function getMinute($timestamp)
    {
        return date('i', $timestamp);
    }

    public static function getSeconds($timestamp)
    {
        return date('s', $timestamp);
    }

}