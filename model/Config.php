<?php

namespace model\Config;


class Config
{
    const CONFIG_PATH = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.php';

    static private function getConfig()
    {
        if (!file_exists(self::CONFIG_PATH)) {
            return false;
        }
        require self::CONFIG_PATH;

        return $config;
    }

    static function getConfigDB()
    {
        $config = self::getConfig();

        return $config['dataBase'];
    }

    static function getConfigAPI()
    {
        $config = self::getConfig();

        return $config['api'];
    }

}