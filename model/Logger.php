<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 07.12.2016
 * Time: 0:38
 */

namespace model\Logger;


class Logger
{
    const PATH = 'runtime';
    const DIR = 'logs';
    const TYPE_LOG = 'log';
    const TYPE_ERR = 'err';

    public function saveLog($text, $err = false)
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . self::PATH . DIRECTORY_SEPARATOR . self::DIR;

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $type = self::TYPE_LOG;

        if (!$err) {
            $type = self::TYPE_ERR;
        }

        $fileName = $path . DIRECTORY_SEPARATOR . date('Y-m-d') . '.' . $type;


        $log = '============= START ' . date('H:i:s') . '=============' . PHP_EOL;
        $log .= $text . PHP_EOL;
        $log .= '============= END =============' . PHP_EOL;

        file_put_contents($fileName, $log, FILE_APPEND);
    }
}