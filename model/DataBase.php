<?php
/**
 * Created by PhpStorm.
 * User: paraf_000
 * Date: 06.12.2016
 * Time: 21:34
 */

namespace model\DataBase;

use model\Config\Config;

class DataBase
{

    const CONFIG_PATH = '..' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.php';

    private $db_host;
    private $db_name;
    private $db_user;
    private $db_password;
    private $link_id;
    private $query_id;
    private $data = array();
    private $errorMessage;


    function __construct()
    {
        $this->connect();
    }

    // Начало транзакции;
    public function begin()
    {
        $this->query("START TRANSACTION");
    }

    // Отмена транзакции;
    public function rollback()
    {
        $this->query("ROLLBACK");
    }

    // Конец транзакции;
    public function commit()
    {
        $this->query("COMMIT");
    }

    // Подключение к базе данных;
    public function connect($db_host = '', $db_name = '', $db_user = '', $db_password = '')
    {

        if ($db_host == '' || $db_name == '' || $db_user == '' || $db_password == '') {
            $this->getConfig();
        } else {
            $this->db_host = $db_host;
            $this->db_name = $db_name;
            $this->db_user = $db_user;
            $this->db_password = $db_password;
        }

        if (empty($this->link_id)) {
            if ($this->link_id = mysqli_connect($this->db_host, $this->db_user, $this->db_password, $this->db_name)) {
                return $this->link_id;
            } else {
                $this->setError('Ошибка подключения к базе данных');
            }
        }
    }

    private function getConfig()
    {
        $config = Config::getConfigDB();

        $this->db_host = $config['db_host'];
        $this->db_name = $config['db_name'];
        $this->db_user = $config['db_user'];
        $this->db_password = $config['db_password'];
    }

    // Запрос к базе данных;
    public function query($sql)
    {
        if (empty($this->link_id)) $this->setError('Нет подключения к базе данных');
        if ($sql == '') $this->setError('Пустой запрос');
        if ($this->query_id = mysqli_query($this->link_id, $sql)) {
            mysqli_next_result($this->link_id);
            return $this->query_id;
        } else {
            $this->error($sql);
        }
    }

    // Загрузка ячейки;
    public function one($sql)
    {
        $this->data = array();
        if ($this->query($sql)) {
            $this->data = mysqli_fetch_array($this->query_id);
            $this->free();
        }
        return $this->data[0];
    }

    // Загрузка строки;
    public function row($sql)
    {
        $this->data = array();
        if ($this->query($sql)) {
            $this->data = mysqli_fetch_array($this->query_id, MYSQLI_ASSOC);
            $this->free();
        }
        return $this->data;
    }

    // Загрузка столбца;
    public function col($sql)
    {
        $this->data = array();
        if ($this->query($sql)) {
            while ($r = mysqli_fetch_array($this->query_id)) {
                $this->data[] = $r[0];
            }
            $this->free();
        }
        return $this->data;
    }

    // Загрузка ассоциативного массива;
    public function assoc($sql)
    {
        $this->data = array();
        if ($this->query($sql)) {
            while ($r = mysqli_fetch_array($this->query_id)) {
                $this->data[$r[0]] = $r[1];
            }
            $this->free();
        }
        return $this->data;
    }

    // Загрузка таблицы+;
    public function all($sql, $type = false, $id = false)
    {
        $this->query($sql);
        $this->data = array();
        while ($row = mysqli_fetch_array($this->query_id, ($type ? MYSQLI_BOTH : MYSQLI_ASSOC))) {
            if ($id) {
                $this->data[$row['id']] = $row;
            } else {
                $this->data[] = $row;
            }
        }
        $this->free();
        return $this->data;
    }

    // Очистка запроса;
    public function free()
    {
        mysqli_free_result($this->query_id);
        $this->query_id = '';
    }

    // Отключение от базы данных;
    public function close()
    {
        mysqli_close($this->link_id);
        $this->link_id = '';
    }

    // Вывод ошибок;
    private function error($sql = '')
    {
        $this->setError('Ошибка в запросе: <i>' . $sql . '</i><br />' . mysqli_errno($this->link_id) . ': ' . mysqli_error($this->link_id));
    }

    public function lastInsertID()
    {
        return mysqli_insert_id($this->link_id);
    }

    public function setError($error = false)
    {
        $this->errorMessage = $error;
    }

    public function getError()
    {
        return $this->errorMessage;
    }

}