<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 07.12.2016
 * Time: 3:20
 */

namespace model\View;


class View
{
    private $data = [];

    private $render = false;

    public $error;

    const PATH = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR;

    public function loadTemplate($template)
    {
        if ($this->checkTemplateExists($template)) {
            $this->render = $this->getPathTemplate($template);
        } else {
            $this->error = 'Error load template';
        }
    }

    public function checkTemplateExists($template)
    {
        if (file_exists($this->getPathTemplate($template))) {
            return true;
        } else {
            return false;
        }
    }

    private function getPathTemplate($template)
    {
        return self::PATH . strtolower($template) . '.php';
    }

    public function assign($variable, $value)
    {
        $this->data[$variable] = $value;
    }

    public function __destruct()
    {
        if ($this->error != '') {
            return false;
        }

        extract($this->data);

        include $this->render;

    }
}